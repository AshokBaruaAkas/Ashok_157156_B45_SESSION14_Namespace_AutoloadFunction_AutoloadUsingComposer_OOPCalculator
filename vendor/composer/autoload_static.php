<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf68a28c07009866844d861d8d092b661
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Tap\\' => 4,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Tap\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP_157156',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP_157156',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf68a28c07009866844d861d8d092b661::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf68a28c07009866844d861d8d092b661::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
