<?php

require_once("../../vendor/autoload.php");

use App\Calculator;

$object = new Calculator();

$object->setNumber1($_POST['number1']);
$object->setNumber2($_POST['number2']);
$object->setOperation($_POST['operation']);

echo $object->getResult();