<?php

function __autoload($classNameWithNamespace){
    $className = explode("\\",$classNameWithNamespace);
    $classPath = "../../src/BITM/SEIP_157156/$className[1].php";
    require_once($classPath);
}

if(empty($_POST['studentID'])){
    echo "I am a Person.<br>";

    $object = new \App\Person();
    $object->setName($_POST['userName']);
    $object->setDateOfBirth($_POST['dateOfBirth']);

    echo $object->getName()."<br>";
    echo $object->getDateOfBirth()."<br>";
}
else{
    echo "I am a Student.<br>";

    $object = new \Tap\Student();

    $object->setName($_POST['userName']);
    $object->setStudentID($_POST['studentID']);
    $object->setDateOfBirth($_POST['dateOfBirth']);

    echo $object->getName()."<br>";
    echo $object->getStudentID()."<br>";
    echo $object->getDateOfBirth()."<br>";
}